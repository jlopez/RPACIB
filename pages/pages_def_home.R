tabHome = fluidPage(align="left",
                    
                    column(width = 12,
                           box(
                             title = "Help",
                             width = NULL,
                             collapsible = TRUE,
                             collapsed = T,
                             status="primary",
                             solidHeader = TRUE,
                             tags$b("Bioinformatics tools: "),
                             p("Select each tool by clicking on the corresponding field in the table. They appear in the field below."),
                             tags$b("CRAN R Packages: "),
                             p("Select each tool by clicking on the corresponding field in the table. They appear in the field below."),
                             tags$b("Bioconductor R Packages: "),
                             p("Select each tool by clicking on the corresponding field in the table. They appear in the field below."),
                             tags$b("Github R Packages: "),
                             p("Select each tool by clicking on the corresponding field in the table. They appear in the field below."),
                             p("Use 'Generate' button to generate your recipe in the preview area then copy/paste the text or use dowload button to get your file."),
                             tags$b("For Singularity containers: To build the image from the recipes file named ubuntu6.10_stacks2.0.simg :"),
                             p("sudo singularity build ubuntu6.10_stacks2.0.simg ubuntu6.10_stacks2.0.recipes"),
                             tags$b("To run a program inside the image container :"),
                             p(" singularity exec ubuntu6.10_stacks2.0.simg python --version "),
                             p("singularity exec ubuntu6.10_stacks2.0.simg python -c 'import pysam; print 2*2'")
                           )),
  
                    
                    column(width = 12,
                           box(
                             id = "boxPackage",
                             title = "Packages and tools",
                             width = NULL,
                             collapsible = TRUE,
                             solidHeader = TRUE,
                             status="primary",
                             tabsetPanel( id='rpackages',
                                          tabPanel('Bioinformatics tools',
                                                   div(id = "formContainer",
                                                       br(),
                                                       selectizeInput('selectedBiocontainer', 'Biocontainer tools selected:', choices = NULL, multiple=TRUE, options = list(maxItems = 30000))),
                                                   
                                                   DT::dataTableOutput('dtbiocontainer')
                                          ),
                                          tabPanel('CRAN Packages', 
                                                   div(id = "formCRAN",
                                                    selectizeInput('selectedCRAN', 'CRAN package selected:', choices = NULL, multiple=TRUE, options = list(maxItems = 30000))
                                                   ,
                                          
                                                    withSpinner(DT::dataTableOutput('dtrcranpackage'), type = 4, proxy.height = "150px")
                                                  )
                                          ),
                                          tabPanel('Bioconductor Packages',   
                                            div(id = "formBioconductor",
                                            #  br(),
                                            #selectizeInput('selectedBioconductor', 'R Bioconductor selected:', choices = NULL, multiple=TRUE, options = list(maxItems = 30000)),
                                            selectizeInput('selectedConductor', 'Bioconductor package selected:', choices = NULL, multiple=TRUE, options = list(maxItems = 30000)),
                                            
                                            DT::dataTableOutput('dtrbioconductorpackage'))),
                                          tabPanel('Github R Packages',
                                            textInput("inputGithub", "Package name:", ""),
                                            actionButton("findGithub", label = "Find", icon("paper-plane"), style="color: #fff; background-color: #337ab7; border-color: #2e6da4"),
                                            div(id = "formGithub",
                                            br(), br(),
                                            selectizeInput('rgithubpackagelist', 'R Github selected:', choices = NULL, multiple=TRUE, options = list(maxItems = 30000)),
                                            
                                            br(), br(),
                                            DT::dataTableOutput('dtrgithubpackage'))),
                                         
                                          tabPanel('Workflows',
                                            div(id = "formWorkflows",
                                              br(),
                                              selectizeInput('selectedWorkflows', 'Workflows selected:', choices = NULL, multiple=TRUE, options = list(maxItems = 30000))),
                                                              
                                              DT::dataTableOutput('dtWorkflows'))
                                          
                             )
                          )
                    ),
                    column(width = 12,
                           box(
                             title = "Preview File:",
                             width = NULL,
                             collapsible = TRUE,
                             solidHeader = TRUE,
                             status="primary",
                             style='height:1000px; overflow-y: scroll',
                             textAreaInput("previewContainer", label = "", height = "1000px")
                           )
                    )
                    
)