TOTO <<- 0

getPackagesWithTitle <- function() {
       contrib.url("https://cran.rstudio.com/", "source") 
       description <- sprintf("%s/web/packages/packages.rds", 
                              "https://cran.rstudio.com/")
       con <- if(substring(description, 1L, 7L) == "file://") {
            file(description, "rb")
         } else {
              url(description, "rb")
           }
     on.exit(close(con))
      
       db <- readRDS(gzcon(con))
      db[, c("Package", "Title", "Version")]
   }


getWorkflows <- function() {
  
  data <- allWORKFLOWS
  
  size <- 0 
  
  for (tools in data) {
    if(length(tools$install) > 0) {
      size <- size + 1
    }
  }
  
  Tool <- character(size)
  Version <- character(size)
  Author <- character(size)
  Description <- character(size)
  Link <- character(size)
  
  i=1
  for (tools in data) {
    Tool[i] = tools$name
    Version[i] = tools$version
    Author[i] = tools$author
    Description[i] = tools$description
    Link[i] = paste0("<a href='", tools$documentation, "'>documentation</a>")
    i = i + 1
  }

  
  result <- data.frame(Workflow=Tool,
                       Version=Version,
                       Author=Author,
                       Description=Description,
                       Link=Link)
  
  return(result)
  
  
}


getBioconductorPackage <- function() {

  data <- allBIOCONTAINER
  
  size <- 0 
  
  for (tools in data) {
    if(length(tools$install) > 0) {
      size <- size + 1
    }
  }
  
  Tool <- character(size)
  Version <- character(size)
  Description <- character(size)
  Link <- character(size)

  i=1
  for (tools in data) {
    
    #if(length(tools$install) > 0) {
    
    Tool[i] = tools$name
    Version[i] = tools$version
    Description[i] = tools$description
    Link[i] = paste0("<a href='", tools$documentation, "'>documentation</a>")
    i = i + 1
    #}
  }
  
  # Link = c("<a href='https://github.com/r78v10a07/DiffExpIR'>documentation</a>",
  #          "<a href='https://github.com/bcgsc/abyss#abyss'>documentation</a>",
  #          "<a href='https://www.zfmk.de/dateien/atoms/files/aliscore_v.2.0_manual_0.pdf'>documentation</a>"
  #          
  # )
  
  result <- data.frame(BioContainer_Tool=Tool,
                       Version=Version,
                       Description=Description,
                       Link=Link)
  
  return(result)
}

getInstallToolPackageWorkflow <- function(tool, containerType) {
  
  data <- allWORKFLOWS
  
  size <- length(data)
  
  intalls <- ""
  res <- c()
  
  for (tools in data) {
    splitB <- strsplit(tool, "%")  
    name <- splitB[[1]][1]
    version <- splitB[[1]][2]
    
    if(tools$name == name) {
      if(tools$version == version) {
        for (c in tools$dependencies) {
          tt <- paste0(c$name, "%", c$version)
          res <- c(res, getInstallToolPackageBioContainer(tt, containerType))
        }
        res <- c(res, paste0("\t", tools$install, collapse='\n' ))
        return(res)
      }
    }
  }
  
  return("\t")
  
}

getInstallToolPackageBioContainer <- function(tool, containerType) {
  
  data <- allBIOCONTAINER

  size <- length(data)
  
  intalls <- ""
  
  for (tools in data) {
      splitB <- strsplit(tool, "%")  
      name <- splitB[[1]][1]
      version <- splitB[[1]][2]
      
      if(tools$name == name) {
        if(tools$version == version) {
          
                      
          
          res <- paste0("\t", tools$install, collapse='\n' )
          return(res)
        }
      }
  }

  return("\t")

}

getInstallToolEnvBioContainer <- function(tool, containerType) {
  
  data <- allBIOCONTAINER
  
  size <- length(data)
  
  intalls <- ""
  
  for (tools in data) {
    splitB <- strsplit(tool, "%")  
    name <- splitB[[1]][1]
    version <- splitB[[1]][2]
    
    if(tools$name == name) {
      if(tools$version == version) {
        if(is.null(tools$env)) {
          return (FALSE)
        } else {
          return(tools$env)
        }
      }
    }
  }
  
  return (FALSE)
}

getInstallToolEnvWorkflows <- function(tool, containerType) {
  
  data <- allWORKFLOWS
  
  size <- length(data)
  
  intalls <- ""
  
  for (tools in data) {
    splitB <- strsplit(tool, "%")  
    name <- splitB[[1]][1]
    version <- splitB[[1]][2]
    
    if(tools$name == name) {
      if(tools$version == version) {
        if(is.null(tools$env)) {
          return (FALSE)
        } else {
          return(tools$env)
        }
      }
    }
  }
  
  return (FALSE)
}




