
output$dtrcranpackage <- DT::renderDataTable({
  future({
    cran <- as.data.frame(getPackagesWithTitle())
    cran
  }) %...>% (function(result) {
    allCRAN <<- result
    return(result)
  })
}, filter='top', escape = FALSE, rownames= FALSE,server = TRUE)

output$dtrbioconductorpackage <- DT::renderDataTable({
  future({
    bioc <- as.data.frame(available.packages(repo = BiocManager::repositories()[1])[, c("Package", "Version")])
    bioc
  }) %...>% (function(result) {
    allBIO <<- result
    return(result)
  })
}, filter='top', escape = FALSE, rownames= FALSE,server = TRUE)

output$dtrgithubpackage <- DT::renderDataTable({
  result <- data.frame(Package=character(),
                             Version=character())
  
  return(result)
}, filter='top', escape = FALSE, rownames= FALSE,server = TRUE)

output$dtbiocontainer <- DT::renderDataTable({
  result <- data.frame(Tool=character(),
                       Version=character(),
                       Description=character())

  result <- getBioconductorPackage()
  
  
  return(result)
}, filter='top', escape = FALSE, rownames= FALSE,server = TRUE)

output$dtWorkflows <- DT::renderDataTable({
  result <- data.frame(Workflow=character(),
                       Author=character(),
                       Version=character(),
                       Description=character())
  
  result <- getWorkflows()
  
  
  return(result)
}, filter='top', escape = FALSE, rownames= FALSE,server = TRUE)


createHeader <- function() {
  result <- ""
  result <- paste0("FROM ", input$fromTemplate)
  result <- paste0(result, "\n")
  return(result)
}

createEnv <- function(result) {
  result <- paste(result, "ARG DEBIAN_FRONTEND=noninteractive","ARG TZ=Europe/Paris", sep="\n")
  result <- paste(result, "export DEBIAN_FRONTEND=noninteractive","export TZ=Europe/Paris", sep="\n")#for singularity

  result <- paste0(result, "\n")
  Rversion = "3.4.3" 
  if(input$rtemplate == "source2") {
    Rversion = "3.4.4" 
  }else if(input$rtemplate == "source3") {
    Rversion = "3.5.0" 
  }else if(input$rtemplate == "source4") {
    Rversion = "3.6.0" 
  }else if(input$rtemplate == "source5") {
    Rversion = "4.0.1" 
  }
  
  selectCRAN <- input$selectedCRAN
  sizeCRAN <- length(selectCRAN)
  selectBIO <- input$selectedConductor
  sizeBIO <- length(selectBIO)
  selectGithub <- input$rgithubpackagelist
  sizeGITHUB <- length(selectGithub)

  if(input$rtemplate == "none" & sizeCRAN == 0 & sizeBIO == 0 & sizeGITHUB == 0) {
    
  } else if(input$rtemplate == "base") {
    result <- paste0(result, "ENV R_VERSION=", Rversion)
    #result <- paste(result, "RUN export R_VERSION", sep = "\n")
    result <- paste(result, "ENV R_CONFIG_DIR=/etc/R/", sep = "\n")
    result <- paste(result, "RUN export R_CONFIG_DIR", sep = "\n")
    
  } else {
    result <- paste(result, "ENV R_VERSION=3.4.3", sep = "\n")
    #result <- paste(result, "RUN export R_VERSION", sep = "\n")
    result <- paste(result, "ENV R_CONFIG_DIR=/etc/R/", sep = "\n")
    result <- paste(result, "RUN export R_CONFIG_DIR", sep = "\n")
  }
  
  haveB <- FALSE
  if(!is.null(input$dtbiocontainer_rows_all)) {
    haveB <- TRUE
    result <- paste(result, "RUN export PATH=/opt/conda/bin:$PATH", sep = "\n")
    result <- paste(result, "RUN export PATH=/opt/biotools/bin:$PATH", sep = "\n")
    result <- paste(result, "RUN export ROOTSYS=/opt/biotools/root", sep = "\n")
    result <- paste(result, "RUN export LD_LIBRARY_PATH='$LD_LIBRARY_PATH:$ROOTSYS/lib'", sep = "\n")
    
    result <- createPathBiocontainer(result)
  }
   if(!is.null(input$dtWorkflows_rows_all)) {
    if(!haveB) {
      result <- paste(result, "RUN export PATH=/opt/conda/bin:$PATH", sep = "\n")
      result <- paste(result, "RUN export PATH=/opt/biotools/bin:$PATH", sep = "\n")
      result <- paste(result, "RUN export ROOTSYS=/opt/biotools/root", sep = "\n")
      result <- paste(result, "RUN export LD_LIBRARY_PATH='$LD_LIBRARY_PATH:$ROOTSYS/lib'", sep = "\n")
    }
    result <- paste(result, "RUN export PATH=/opt/workflows/bin:$PATH", sep = "\n")
    result <- createPathWorkflow(result)
  }
  

  result <- paste0(result, "\n")
  return(result)
}

createLabel <- function(result) {
  
      result <- paste(result, "LABEL Author YourName", sep = "\n")
      result <- paste(result, "LABEL Version v0.0.1", sep = "\n")
      result <- paste0(result, "\n", "LABEL build_date ", format(Sys.time(), "%Y %b %d")) 

    result <- paste0(result, "\n")
    return(result)
}

createExect <- function(result) {

    result <- paste(result, "CMD exec /bin/bash \"$@\"", sep = "\n")
    result <- paste0(result, "\n")
    return(result)
}

createLibPrePost <- function(result) {
  
      result <- paste(result, "RUN apt-get update", sep = "\n")
      
      result <- paste(result, "RUN apt-get install -y wget libblas3 libblas-dev liblapack-dev liblapack3 curl", sep = "\n")
      result <- paste(result, "RUN apt-get install -y gcc fort77 aptitude", sep = "\n")
      result <- paste(result, "RUN aptitude install -y g++ xorg-dev libreadline-dev  gfortran", sep = "\n")
      result <- paste(result, "RUN apt-get install -y libssl-dev libxml2-dev libpcre3-dev liblzma-dev libbz2-dev libcurl4-openssl-dev openjdk-8-jre", sep = "\n")
      result <- paste(result, "RUN apt-get install -y  autotools-dev automake cmake curl grep sed dpkg fuse git zip openjdk-8-jre build-essential pkg-config python3 python3-dev python3-pip bzip2 ca-certificates libglib2.0-0 libxext6 libsm6 libxrender1 mercurial subversion zlib1g-dev libncurses5-dev libncursesw5-dev",sep="\n")

      result <- paste(result, "RUN apt-get update", sep = "\n")
      result <- paste(result, "ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64", sep = "\n")
       
      result <- paste0(result, "\n")
  
  return(result)
}

#' Use for create install R from RBase content
createRBase <- function(result) {
  
  result <- paste(result, '############### Install R From RBase ##############', sep = "\n")
  result <- paste(result, "RUN apt-get install -y r-base r-base-dev", sep = "\n")
  result <- paste0(result, "\n")
  result <- paste0(result, "ENV R_VERSION=$(Rscript -e 'vers=paste(R.Version()[c(\"major\", \"minor\")], collapse = \".\"); cat(vers)')")
  result <- paste0(result, "\n")
  return(result)
}

#' Use for create install R from Source content
createRSource <- function(result, Rversion) {
  
  result <- paste(result, '############### Install R From Source ##############', sep = "\n")
  
    result <- paste(result, 'RUN cd $HOME \\', sep = "\n")
    if( Rversion == "4.0.1")
    result <- paste0(result, '\n\t&& wget https://cran.rstudio.com/src/base/R-4/R-', Rversion, '.tar.gz \\')
    else
    result <- paste0(result, '\n\t&& wget https://cran.rstudio.com/src/base/R-3/R-', Rversion, '.tar.gz \\')

    result <- paste0(result, '\n\t&& tar xvf R-', Rversion, '.tar.gz \\')
    result <- paste0(result, '\n\t&& cd R-', Rversion, ' \\')
    result <- paste(result, "\t&& ./configure --enable-R-static-lib --with-blas --with-lapack --enable-R-shlib=yes \\", sep = "\n") 
    result <- paste(result, "\t&& make \\", sep = "\n") 
    result <- paste(result, "\t&& make install \\", sep = "\n") 
    result <- paste0(result, "\n")
  return(result)
}

#' Use for create install R from CRAN content
createRCran <- function(result) {
  
  result <- paste(result, '############### Install R From CRAN ##############', sep = "\n")
  result <- paste(result, "RUN apt-get install -y software-properties-common", sep = "\n")
  result <- paste(result, "RUN add-apt-repository 'deb http://cloud.r-project.org/bin/linux/ubuntu xenial/'", sep = "\n")
  result <- paste(result, "RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E084DAB9", sep = "\n")
  result <- paste(result, "RUN apt-get update", sep = "\n")
  result <- paste(result, "RUN apt-get install -y r-base r-base-dev", sep = "\n")
  
  result <- paste0(result, "\n")
  return(result)
}

#' Use for create CRAN content
createCRANPackage <- function(result) {
  
  selectCRAN <- input$selectedCRAN
  sizeCRAN <- length(selectCRAN)
  
  if(sizeCRAN > 0) {
    if(sizeCRAN < length(allCRAN[,"Package"])) { 
      if(sizeCRAN >= 1) { 
        
        result <- paste(result, '############### Install CRAN Package ##############', sep = "\n")
        
        listRCRAN <- 'RUN R --slave -e "install.packages( c('
        
        for (pkg in 1:sizeCRAN){
          if(pkg < sizeCRAN) {
            listRCRAN <- paste0(listRCRAN, "'",selectCRAN[pkg],"', ")
          } else {
            listRCRAN <- paste0(listRCRAN, "'",selectCRAN[pkg],"'), repos='https://cloud.r-project.org'))\"")
          }
        }
        
        result <- paste(result, listRCRAN, sep = "\n")
        result <- paste0(result, "\n")
      }
    }
  }
  
  return(result)
}

#' Use for create Github content
createGithubPackage <- function(result) {
  
  if(!is.null(input$rgithubpackagelist)) {
  
    selectGithub <- input$rgithubpackagelist
    sizeGITHUB <- length(selectGithub)
    
    if(sizeGITHUB >= 1) {
      
      result <- paste(result, '############### Install Github Package ##############', sep = "\n")
      
      listRGITHUB <- 'RUN R --slave -e "install_github(c('
      
      for (pkg in 1:sizeGITHUB){
        if(pkg < sizeGITHUB) {
          listRGITHUB <- paste0(listRGITHUB, '\'',selectGithub[pkg],'\', ')
        } else {
          listRGITHUB <- paste0(listRGITHUB, '\'',selectGithub[pkg],'\'))"')
        }
      }
      result <- paste(result, listRGITHUB, sep = "\n")
      
    }
  }
  

  
  result <- paste0(result, "\n")
  return(result)
}

#' Use for create Bioconductor content
createBioconductorPackage <- function(result) {
  
  selectBIO <- input$selectedConductor
  sizeBIO <- length(selectBIO)
  if(!is.null(sizeBIO)) {
    if(sizeBIO < length(allBIO[,"Package"])) { 
      if(sizeBIO >= 1) { 
        
        result <- paste(result, '############### Install Bioconductor Package ##############', sep = "\n")
        
        
        result <- paste(result, 'RUN R --slave -e "source(\'https://bioconductor.org/biocLite.R\'); biocLite(); "', sep = "\n")
        listRBIO <- 'RUN R --slave -e "source(\'https://bioconductor.org/biocLite.R\'); biocLite('
        
      
        for (pkg in 1:sizeBIO){
          if(pkg < sizeBIO) {
            listRBIO <- paste0(listRBIO, '\'',selectBIO[pkg],'\', ')
          } else {
            listRBIO <- paste0(listRBIO, '\'',selectBIO[pkg],'\')"')
          }
        }
        
        result <- paste(result, listRBIO, sep = "\n")
        result <- paste0(result, "\n")
      }
    }
  }
  
  return(result)
}


createPathWorkflow <- function(result) {
  
    selectBioTool <- input$selectedWorkflows
    for (tool in selectBioTool){
      
      to <- getInstallToolEnvWorkflows(tool, input$containerType)
      
      if(is.character(to)) {
        result <- paste0(result, "\nRUN ", to)
      }
      
    }
    
  return(result)
  
}


createPathBiocontainer <- function(result) {

    selectBioTool <- input$selectedBiocontainer
    for (tool in selectBioTool){
      
      to <- getInstallToolEnvBioContainer(tool, input$containerType)
      
      if(is.character(to)) {
        result <- paste0(result, "\nRUN ", to)
      }
      
    }
    
  return(result)
  
}
#' Use for create Workflows content
createWorkflows <- function(result, haveR, haveConda) {
  
   selectWorkflows <- input$selectedWorkflows
    
    result <- paste0(result, "\n")
    
    if(!haveConda) {
    
   
        
        result <- paste0(result, "\n")
        
        result <- paste(result, "RUN mkdir /opt/biotools", sep = "\n")
        result <- paste(result, "RUN mkdir /opt/biotools/bin", sep = "\n")
        result <- paste(result, "RUN chmod 777 -R /opt/biotools/", sep = "\n")
        result <- paste(result, "ENV PATH=/opt/biotools/bin:${PATH}", sep = "\n")
        

        
        result <- paste0(result, "\n")
        
        if(!haveR) {
          result <- paste(result, "RUN conda config --add channels r", sep = "\n")
        }
        
       
    
    }
    
    result <- paste(result, "RUN mkdir /opt/workflows", sep = "\n")
    result <- paste(result, "RUN mkdir /opt/workflows/bin", sep = "\n")
    result <- paste(result, "RUN chmod 777 -R /opt/workflows/", sep = "\n")
    result <- paste(result, "ENV PATH=/opt/workflows/bin:${PATH}", sep = "\n")
    
    for (tool in selectWorkflows){
      
      result <- paste(result, '############### Install Workflow ##############\n', sep = "\n")
      
      for(i in getInstallToolPackageWorkflow(tool, input$containerType)) {
        result <- paste0(result, "RUN", i, "\n")
      }
    }
    
  
  
  result <- paste0(result, "\n")
  return(result)
  
}

#' Use for create Biocontainer content
createBiocontainer <- function(result, haveR) {

 
    selectBioTool <- input$selectedBiocontainer
    
    result <- paste0(result, "\n")
    
    result <- paste0(result, "\n")
    
    result <- paste(result, "RUN mkdir /opt/biotools", sep = "\n")
    result <- paste(result, "RUN mkdir /opt/biotools/bin", sep = "\n")
    result <- paste(result, "RUN chmod 777 -R /opt/biotools/", sep = "\n")
    result <- paste(result, "ENV PATH=/opt/biotools/bin:${PATH}", sep = "\n")
    
    
    if(!haveR) {
      result <- paste(result, "RUN conda config --add channels r", sep = "\n")
    }
    

    result <- paste(result, '############### Install BioContainer tools ##############\n', sep = "\n")
    
    for (tool in selectBioTool){
      result <- paste0(result, "RUN ", getInstallToolPackageBioContainer(tool, input$containerType),"\n")
    }

    result <- paste0(result, "\n")
    return(result)
}

#' Use for create Singularity or Dockerfile
createContentFile <- function() {
  
  haveR = FALSE

  result <- createHeader()
  result <- createEnv(result)
  result <- createLabel(result)
  result <- createLibPrePost(result)

  selectCRAN <- input$selectedCRAN
  sizeCRAN <- length(selectCRAN)

  selectBIO <- input$selectedConductor
  sizeBIO <- length(selectBIO)

  selectGithub <- input$rgithubpackagelist
  sizeGITHUB <- length(selectGithub)

  if (!is.null(sizeCRAN) ) print(sizeCRAN)
  if(input$rtemplate != "none" | sizeCRAN > 0 | sizeBIO > 0 | sizeGITHUB > 0) {
    
      haveR = TRUE
    
      if(input$rtemplate == "source" || input$rtemplate == "source2" || input$rtemplate == "source3" || input$rtemplate == "source4" || input$rtemplate == "source5") {
          Rversion = "3.4.3" 
          if(input$rtemplate == "source2") {
            Rversion = "3.4.4" 
          }else if(input$rtemplate == "source3") {
            Rversion = "3.5.0" 
          } else if(input$rtemplate == "source4") {
            Rversion = "3.6.0" 
          } else if(input$rtemplate == "source5") {
            Rversion = "4.0.1" 
          }
          result <- createRSource(result, Rversion)
      } else if(input$rtemplate == "base" | input$rtemplate == "none") {
          result <- createRBase(result)
      } else if(input$rtemplate == "cran") {
          result <- createRCran(result)
      }
      
  }
      
  result <- createCRANPackage(result)
  result <- createBioconductorPackage(result)
  result <- createGithubPackage(result)

  haveBiocontainer <- FALSE
  if(!is.null(input$selectedBiocontainer)) {
    haveBiocontainer <- TRUE
    result <- createBiocontainer(result, haveR)
  }
  
  result <- createWorkflows(result, haveR, haveBiocontainer)
  
  result <- createExect(result)
  
  result <- paste(result, input$customDataContainer, sep = "\t\n")
  
  return(result)
}

observeEvent(input$createContainer, {

  js$collapse("boxPackage")
  
  show("downloadContainerFile")
  
  result <- createContentFile()
  
  if(input$containerType == "singularity") {
  # will convert result using spython https://singularityhub.github.io/
  #save result in a tmp file
  dockerfile = tempfile(pattern = "Dockerfile")
  tmpSingularityFile = tempfile(pattern = "Singularity")
  write.table(result, dockerfile, row.names=F, col.names=F, quote=F)
  cmd = paste0("sed -i 's/R_VERSION=.*/R_VERSION=getRversion/' ",dockerfile) 
  system(cmd)
  cmd = paste0("spython recipe ", dockerfile, " > ", tmpSingularityFile)
  system(cmd)
  # read tmpSingularityFile in result var
  cmd = paste0("sed -i 's/getRversion/$(Rscript -e \"vers=paste(R.Version()[c(\"major\",\"minor\")],collapse =\".\");cat(vers)\")/' ", tmpSingularityFile)
  system(cmd)

  result <- paste(readLines(tmpSingularityFile, n=-1),"\n", collapse = '')
  unlink(dockerfile)
  unlink(tmpSingularityFile)

  }
  updateTextAreaInput(session, "previewContainer",
                      label = "",
                      value = result)
  
})

#createContainerPackage <- eventReactive(input$createContainer, {

  #result <- createContentFile()
  
  #show("downloadContainerFile")
  

  #HTML(result)

#})



#output$previewContainer <- renderText({
#  createContainerPackage()
#})


output$downloadContainerFile <- downloadHandler(
  filename = function() {
    name <- ""
     name <- paste("Dockerfile",input$imageName, sep = ".")
    
  },
  content = function(file) {
    result <- input$previewContainer
    write(result,file=file)
  }
)

observe({
  
  if(is.null(input$dtbiocontainer_rows_selected)) {
    shinyjs::reset("formContainer")
  } else {
    i = 1
    selectBiocontainer <- list()
    for(x in input$dtbiocontainer_rows_selected) {
      selectBiocontainer[i] = paste(allBIOCONTAINER[[x]]$name, allBIOCONTAINER[[x]]$version, sep = "%")
      i = i + 1
    }
    selectBiocontainer <- c(unlist(selectBiocontainer))
    updateSelectizeInput(session,"selectedBiocontainer", choices = selectBiocontainer, selected = selectBiocontainer, options = list())
  }
})




observe({
  
  if(is.null(input$dtWorkflows_rows_selected)) {
    shinyjs::reset("formWorkflows")
  } else {
    i = 1
    selectWorkflows <- list()
    for(x in input$dtWorkflows_rows_selected) {
      selectWorkflows[i] = paste(allWORKFLOWS[[x]]$name, allWORKFLOWS[[x]]$version, sep = "%")
      i = i + 1
    }
    selectWorkflows <- c(unlist(selectWorkflows))
    updateSelectizeInput(session,"selectedWorkflows", choices = selectWorkflows, selected = selectWorkflows, options = list())
  }
})


observe({
  
  if(is.null(input$dtrcranpackage_rows_selected)) {
    shinyjs::reset("formCRAN")
  } else {
    i = 1
    selectCRAN <<- list()
    for(x in input$dtrcranpackage_rows_selected) {
      selectCRAN[i] <<- as.character(allCRAN$Package[x])
      i = i + 1
    }
    #selectCRAN <- c(unlist(selectCRAN))
    updateSelectizeInput(session,"selectedCRAN", choices = selectCRAN, selected = selectCRAN, options = list())
    
  }
  
})

observe({
  
  if(is.null(input$dtrbioconductorpackage_rows_selected)) {
    shinyjs::reset("formBioconductor")
  } else {
    i = 1
    selectConductor <<- list()
    for(x in input$dtrbioconductorpackage_rows_selected) {
      selectConductor[i] <<-as.character(allBIO$Package[x])
      i = i + 1
    }

    updateSelectizeInput(session,"selectedConductor", choices = selectConductor, selected = selectConductor, options = list())
    
  }
  
})


observe({
  if(is.null(input$dtrgithubpackage_rows_selected)) {
    
    selectGithub <- input$rgithubpackagelist
    sizeGITHUB <- length(selectGithub)
    
    if(sizeGITHUB < 1) {
      shinyjs::reset("formGithub")
    }

  } else {
    i = 1
    selectG <- list()
    for(x in input$dtrgithubpackage_rows_selected) {
      selectG[i] = toString(allGITHUB$Package[[x]])
      i = i + 1
    }
    selectG <- c(unlist(selectG))
    
    selectGithub <- input$rgithubpackagelist
    sizeGITHUB <- length(selectGithub)
    
    if(sizeGITHUB > 1) {
      selectG <- c(selectG, selectGithub)
    }
  
    updateSelectizeInput(session,"rgithubpackagelist", choices = selectG, selected = selectG, options = list())
  }
})

observeEvent(input$findGithub, {
  
  name <- input$inputGithub
  
  if(!stri_isempty(name)) {
    future({
      github <-  data.frame(Package = gh_suggest(name, keep_title = FALSE), Title = attr(gh_suggest(name, keep_title = TRUE), "title"))
      github
    }) %...>% (function(result) {
      allGITHUB <<- result
      if(length(allGITHUB)  >= 1 ) {
        output$dtrgithubpackage <- DT::renderDataTable({
          result <- allGITHUB
          return(result)
        }, filter='top', escape = FALSE, rownames= FALSE,server = TRUE)
      }
    })
  }
})


